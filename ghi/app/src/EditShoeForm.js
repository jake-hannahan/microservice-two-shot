import { useState, useEffect } from "react"
import { useLocation } from "react-router-dom"
import background from './SneakerBackground.jpg'

function EditShoeForm (props) {
  const shoe = useLocation()
  console.log(shoe)

  const [formData, setFormData] = useState({
    manufacturer: shoe.state.shoe.manufacturer,
    modelName: shoe.state.shoe.model_name,
    color: shoe.state.shoe.color,
    imageUrl: shoe.state.shoe.image_url,
    bin: shoe.state.shoe.bin,
    id: shoe.state.shoe.id
  })

  const [bins, setBins] = useState([])

  async function fetchData () {
    const response = await fetch('http://localhost:8100/api/bins/')
    if (response.ok) {
      const data = await response.json()
      setBins(data.bins)
    } else {
      console.error(response)
    }

  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    const data = {
      manufacturer: formData.manufacturer,
      model_name: formData.modelName,
      color: formData.color,
      image_url: formData.imageUrl,
      bin: formData.bin
    }

    const shoeUrl = `http://localhost:8080/api/shoes/${shoe.state.shoe.id}/`
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    }
    const response = await fetch(shoeUrl, fetchConfig)
    if (response.ok) {
      const newShoe = await response.json()
      console.log("new shoe: ", newShoe)

      window.location.replace('http://localhost:3000/shoes')
      // setFormData({
      //   manufacturer: "",
      //   modelName: "",
      //   color: "",
      //   imageUrl: "",
      //   bin: ""
      // })
    }
  }

  const handleFieldChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <>
    <div  style={{backgroundImage: `url(${background})`, backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
      <div style={{backgroundColor: 'rgb(255, 255, 255, .78)' ,backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>

      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4 rounded" style={{backgroundColor: 'white'}}>
            <h1 className="display-5 text-center">Edit shoe entry</h1>
            <form onSubmit={handleSubmit} id="creates-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.modelName} placeholder="Model" required type="text" name="modelName" id="model" className="form-control"/>
                <label htmlFor="model">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.color} placeholder="Primary color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Primary color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.imageUrl} placeholder="Image Url" required type="text" name="imageUrl" id="imageUrl" className="form-control"/>
                <label htmlFor="imageUrl">Image Url</label>
              </div>
              <div className="form-floating mb-3">
                <select onChange={handleFieldChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {bins.map(bin => {
                    return (
                      <option value={bin.href} key={bin.href}>Closet: {bin.closet_name} | Bin #: {bin.bin_number} | Bin Size: {bin.bin_size}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Save</button>
            </form>
          </div>
        </div>
      </div>
      </div>
      </div>
    </>
  )
}

export default EditShoeForm;
