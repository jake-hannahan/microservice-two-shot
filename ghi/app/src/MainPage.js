import { Link } from 'react-router-dom'
import background from './MainBackground.jpg'

function MainPage() {
  return (
    <>
    <div  style={{backgroundImage: `url(${background})`, backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
      <div style={{backgroundColor: 'rgb(255, 255, 255, .78)' ,backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
      <div className="container">
        <div className="px-4 py-5] text-center" style={{ paddingTop: 90, marginBotton: 48}}>
          <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              Need to keep track of your shoes and hats? We have
              the solution for you!
            </p>
          </div>
        </div>
      </div>
      <div className="d-grid gap-2 text-center" style={{height: 50, display: 'flex', justifyContent:'center', alignItems:'center'}}>
        <Link to="/shoes" style={{alignSelf: 'flex-start'}}>
          <button className="btn btn-primary btn-lg btn-block" type="button" style={{height: 50, width: 600}}>
            Your Shoes
          </button>
        </Link>
        <Link to="/shoes/new">
          <button className="btn btn-secondary btn-lg btn-block" type="button" style={{height: 50, width: 600}}>
            Add Shoe
          </button>
        </Link>
        <Link to="/hats">
          <button className="btn btn-primary btn-lg btn-block" type="button" style={{height: 50, width: 600}}>
            Your Hats
          </button>
        </Link>
        <Link to="/hats/new">
          <button className="btn btn-secondary btn-lg btn-block" type="button" style={{height: 50, width: 600}}>
            Add Hat
          </button>
        </Link>
      </div>
      </div>
    </div>
    </>
  );
}

export default MainPage;
