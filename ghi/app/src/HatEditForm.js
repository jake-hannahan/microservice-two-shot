import React, { useEffect, useState, } from 'react';
import { useLocation } from 'react-router-dom';
import background from './HatBackground.png';

function HatEditForm(props) {
    const hat = useLocation();

    const [formData, setFormData] = useState({
        fabric: hat.state.hat.fabric,
        styleName: hat.state.hat.style_name,
        color: hat.state.hat.color,
        pictureUrl: hat.state.hat.picture_url,
        location: hat.state.hat.location,
        id: hat.state.hat.id
      })

    const [locations, setLocations] = useState([]);

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/locations/')

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        } else {
            console.error(response);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            fabric: formData.fabric,
            style_name: formData.styleName,
            color: formData.color,
            picture_url: formData.pictureUrl,
            location: formData.location
        };

        const hatUrl = `http://localhost:8090/api/hats/${hat.state.hat.id}/`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            window.location.replace('http://localhost:3000/hats');
        }
    }

    const handleFieldChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div  style={{backgroundImage: `url(${background})`, backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
        <div style={{backgroundColor: 'rgb(255, 255, 255, .78)' ,backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4 rounded" style={{backgroundColor: "white"}}>
            <h2 className='display-5 text-center'>Edit a hat</h2>
                <form onSubmit={handleSubmit} id="edit-hat-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFieldChange} type="text" name="fabric" required
                        id="fabric" value={formData.fabric} className="form-control" />
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFieldChange} placeholder="Style name" required
                        type="text" name="styleName" id="styleName" value={formData.styleName} className="form-control"/>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFieldChange} placeholder="Color" required
                        type="text" name="color" id="color" value={formData.color} className="form-control"/>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFieldChange} placeholder="Picture Url" required
                        type="url" name="pictureUrl" id="pictureUrl" value={formData.pictureUrl} className="form-control" />
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFieldChange} value={formData.location} required id="location" className="form-select" name="location">
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>
                                        Closet: {location.closet_name} | Section #: {location.section_number} | Shelf #: {location.shelf_number}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Save</button>
                </form>
            </div>
            </div>
        </div>
        </div>
        </div>
    )

};

export default HatEditForm;
