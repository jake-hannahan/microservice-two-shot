import { NavLink } from 'react-router-dom';
import logo from './WardrobifyLogo.png'

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg" style={{backgroundColor: "rgba(21, 20, 20, 0.9)"}}>
      <div className="container-fluid">
          <NavLink className="navbar-brand" to="/" style={{width: '20%'}}>
          <img src={logo} alt="logo" style={{width: '100%'}}/>
          </NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/" style={{color: "white"}}>Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/shoes" style={{color: "white"}}>
                Shoes
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/shoes/new" style={{color: "white"}}>New shoe</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/hats" style={{color: "white"}}>Hats</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/hats/new" style={{color: "white"}}>New Hat</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
