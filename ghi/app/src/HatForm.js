import React, { useEffect, useState, } from 'react';
import background from './HatBackground.png';

function HatForm() {
    const [locations, setLocations] = useState([]);
    const [fabric, setFabric] = useState('');
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;

        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            setFabric("");
            setStyleName("");
            setColor("");
            setPictureUrl("");
            setLocation("");
        }
    }
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/locations/')

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div  style={{backgroundImage: `url(${background})`, backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
        <div style={{backgroundColor: 'rgb(255, 255, 255, .78)' ,backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4 rounded" style={{backgroundColor: "white"}}>
            <h2 className='display-5 text-center'>Add hat</h2>
                <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" value={fabric} className="form-control"/>
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleStyleNameChange} placeholder="Style name" required type="text" name="styleName" id="styleName" value={styleName} className="form-control"/>
                        <label htmlFor="styleName">Style Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" value={color} className="form-control"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureUrlChange} placeholder="Picture Url" required type="url" name="pictureUrl" id="pictureUrl" value={pictureUrl} className="form-control"/>
                        <label htmlFor="pictureUrl">Picture Url</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleLocationChange} required id="location" className="form-select" value={location} name="location">
                            <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            Closet: {location.closet_name} | Section #: {location.section_number} | Shelf #: {location.shelf_number}
                                        </option>
                                    )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        </div>
        </div>
    )

};

export default HatForm;
