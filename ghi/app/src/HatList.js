import React, { useEffect, useState, } from 'react';
import { Link } from "react-router-dom";
import background from './HatBackground.png';

function HatList() {
    const [hats, setHats] = useState([]);

    const handleClick = async (e) => {
        const hatId = e.target.value;
        const hatUrl = `http://localhost:8090/api/hats/${hatId}/`;
        const fetchConfig = {
            method: "DELETE",
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            console.log(response.json());
            fetchData();
        }
    }


    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/')

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
    <div  style={{backgroundImage: `url(${background})`, backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
        <div style={{backgroundColor: 'rgb(255, 255, 255, .78)' ,backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
        <div className='container'>
        <h2 className='display-2 text-center'><b>YOUR HATS</b></h2>
            <table className="table table-striped rounded" style={{backgroundColor: "white"}}>
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Style name</th>
                        <th>Color</th>
                        <th>Location</th>
                        <th>Picture</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                        return (
                            <tr key={hat.href} value={hat.href}>
                                <td>{hat.fabric}</td>
                                <td>{hat.style_name}</td>
                                <td>{hat.color}</td>
                                <td>{hat.location}</td>
                                <td>
                                    <img src={hat.picture_url} className="img-thumbnail" style={{height: 100, width: 100}} alt='' />
                                </td>
                                <td>
                                <div className="dropdown me-1">
                                    <button type="button" className="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" data-bs-offset="10,20">
                                        Options
                                    </button>
                                    <ul className="dropdown-menu">
                                        <Link to='/hats/edit' state={{ hat : hat}} className="text-decoration-none">
                                        <li value={hat.id} className="dropdown-item">
                                            Edit
                                        </li>
                                        </Link>
                                        <li onClick={handleClick} value={hat.id} className="btn dropdown-item link-danger">Delete</li>
                                    </ul>
                                </div>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
        </div>
    </div>
    )
    }
export default HatList;
