import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import background from './SneakerBackground.jpg'
function ShoeList () {
  const [shoes, setShoes] = useState([])

  async function fetchData () {
    const response = await fetch('http://localhost:8080/api/shoes')
    if (response.ok) {
      const data = await response.json()
      setShoes(data.shoes)
    } else {
      console.error(response)
    }

  }


  const handleDelete = async (e) => {
    const shoeId =  e.target.value

    const shoeUrl = `http://localhost:8080/api/shoes/${shoeId}`
    const fetchConfig = {
      method: "delete",
      body: "",
      headers: {
        'Content-Type': 'application/json',
      }
    }

    const response = await fetch(shoeUrl, fetchConfig)
    if (response.ok) {
      console.log("Deleted")
      fetchData();
    }
  }


  useEffect(() => {
    fetchData();
  }, [])

  return (
    <>
      <div  style={{backgroundImage: `url(${background})`, backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
        <div style={{backgroundColor: 'rgb(255, 255, 255, .78)' ,backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
          <div className="container" >
            <div >
              <h2 className="display-2 text-center" ><b>YOUR SHOES</b></h2>
              <table className="table table-striped shadow p-4 mt-4 rounded" style={{backgroundColor: 'white'}}>
                <thead>
                  <tr>
                    <th></th>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Primary color</th>
                    <th>Storage location</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {shoes.map(shoe => {
                    return (
                      <tr key ={shoe.id}>
                        <td>
                          <img src={shoe.image_url} className="img-thumbnail" style={{height: 100, width:100}} alt=""/>
                        </td>
                        <td>{shoe.manufacturer}</td>
                        <td>{shoe.model_name}</td>
                        <td>{shoe.color}</td>
                        <td>Closet: {shoe.bin.closet_name} | Bin: {shoe.bin.bin_number}</td>
                        <td>
                        <div className="dropdown me-1">
                          <button type="button" className="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" data-bs-offset="10,20">
                            Options
                          </button>
                          <ul className="dropdown-menu">
                            <Link to='/shoes/edit' state={{ shoe : shoe }} className="text-decoration-none">
                              <li value={shoe.id} className="dropdown-item">
                                Edit
                              </li>
                            </Link>
                            <li onClick={handleDelete} value={shoe.id} className="btn dropdown-item link-danger">Delete</li>
                          </ul>
                        </div>
                        </td>
                      </tr>
                    )
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}


export default ShoeList;
