from django.db import models

# Create your models here.



class BinVO (models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()


class Shoe (models.Model):
    model_name = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    image_url = models.URLField()

    bin = models.ForeignKey(
        BinVO,
        related_name= "shoes",
        on_delete=models.CASCADE
      )
