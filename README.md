# Wardrobify

Team:

* Jake - Hats
* Hayden - Shoes

## Design

Styled using bootstrap and inline CSS.

## Shoes microservice

Contains a Shoe model to hold information (manufacturer, model, color, bin, and image url) about each of the shoes added to the application, along with a BinVO model that is automatically refreshed every minute by a poller and is pulled from our wardrobe microservice. Each Shoe is related to one BinVO by a foreign key to show where that shoe is stored.

## Hats microservice

My Hat model has 5 fields. The fabric, style name, color, picture url, and location of a hat. Fabric, style name, and color are CharFields. Picture url is a URLField. While location is a ForeignKey to my LocationVO and this shows where the hat is stored. The LocationVO is initialized in hats_rest.models.py. The poller.py fetches locations from the wardrobe-api microservice and uses the update_or_create method to update or create LocationVOs.
